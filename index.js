/** @format */
import React from 'react'
import { Provider } from 'react-redux'
import { View, StatusBar, AppRegistry } from 'react-native'
import { name as appName } from './app.json'
import store from './src/store'
import StackNavigator from './src/navigation'
import NavigatorService from './src/utils/navigator'

const Root = () => (
  <Provider store={store}>
    <View style={{ flex: 1 }}>
      <StatusBar barStyle="light-content" />
      <StackNavigator
        ref={component => {
          NavigatorService.addContainer('root', component)
        }}
      />
    </View>
  </Provider>
)

AppRegistry.registerComponent(appName, () => Root)
