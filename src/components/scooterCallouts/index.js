import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, View, Text, Image } from 'react-native'

class ScooterCallouts extends React.Component {
  render () {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.bubble}>
          <Text>{this.props.children}</Text>
          <Image
            style={styles.imageDirection}
            source={require('../../assets/images/direction.png')}
          />
        </View>
      </View>
    )
  }
}

ScooterCallouts.propTypes = {
  children: PropTypes.node.isRequired,
  style: PropTypes.object
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    alignSelf: 'flex-start'
  },
  bubble: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'flex-start',
    backgroundColor: '#ffffff',
    paddingHorizontal: 8,
    paddingVertical: 8
  },
  imageDirection: {
    width: 30,
    height: 30,
    tintColor: 'red'
  }
})

export default ScooterCallouts
