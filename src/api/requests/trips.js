import { url } from '../config'

export const getTripsRequest = () => {
  return {
    method: 'get',
    url: `${url}/get-trips`
  }
}
