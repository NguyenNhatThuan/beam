import { url } from '../config'

export const getScootersRequest = () => {
  return {
    method: 'get',
    url: `${url}/get-scooters`
  }
}
