import axios from 'axios'
import { timeout } from './config'

export function * call (request) {
  try {
    request = {
      ...request,
      timeout
    }
    console.log('Request ->>> ', request)
    const response = yield axios(request)
    if (response && response.error) {
      throw new Error(response.error)
    }
    console.log('Response ->>> ', response)
    return response
  } catch (error) {
    console.log(error)
  }
}
