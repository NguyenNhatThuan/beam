const host = 'http://192.168.1.58'
const port = 10000
const url = `${host}:${port}`
const timeout = 10000

export { url, port, timeout }
