import geolib from 'geolib'

export const getGeolocationUtil = (
  successCallback,
  errorCallback = undefined
) => {
  navigator.geolocation.getCurrentPosition(
    position => {
      const { coords } = position
      position.coords = {
        ...coords,
        latitudeDelta: 0.015,
        longitudeDelta: 0.025
      }
      successCallback(position)
    },
    positionError => {
      console.log('Position Error: ', positionError)
      errorCallback && errorCallback(positionError)
    },
    { enableHighAccuracy: true }
  )
}

export const getDistance = (start, target) => {
  return geolib.getDistanceSimple(start, target) / 1000
}
