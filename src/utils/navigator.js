import { NavigationActions, StackActions } from 'react-navigation'
import _ from 'lodash'

let _containers = {}

const addContainer = (key, container) => {
  _containers[key] = container
}

const reset = (key, routeName, params) => {
  _containers[key].dispatch(
    StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName,
          params
        })
      ]
    })
  )
}

const replace = (key, routeName, params, toScreen) => {
  const container = _containers[key]
  const routes = container.state.nav.routes

  if (toScreen) {
    let number = 0
    for (let index = routes.length - 1; index >= 0; index--) {
      const element = routes[index]
      if (element.routeName === toScreen) {
        break
      } else {
        number++
      }
    }
    container.dispatch(
      StackActions.pop({
        n: number,
        immediate: true
      })
    )
  }

  const currentKey = _.last(container.state.nav.routes).key
  container.dispatch(
    StackActions.replace({
      key: currentKey,
      routeName,
      params
    })
  )
}

const navigate = (key, routeName, params) => {
  _containers[key].dispatch(
    NavigationActions.navigate({
      type: 'Navigation/NAVIGATE',
      routeName,
      params
    })
  )
}

const back = key => {
  _containers[key].dispatch(NavigationActions.back())
}

export default {
  addContainer,
  navigate,
  back,
  replace,
  reset
}
