import { all } from 'redux-saga/effects'
import takeScooterSagas from './scooter'
import takeTripSagas from './trip'

function * combineSagas () {
  yield all([takeScooterSagas(), takeTripSagas()])
}

export default combineSagas
