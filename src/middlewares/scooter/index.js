import { getScootersRequest } from '../../api/requests/scooter'
import { call } from '../../api'
import { takeLatest, put } from 'redux-saga/effects'
import { GET_SCOOTERS } from '../../constants/types'
import { getScootersSuccessfully } from '../../screens/home/action'

function * submitGetScooters (action) {
  try {
    const request = getScootersRequest()
    const response = yield call(request)
    yield put(getScootersSuccessfully(response.data))
  } catch (error) {
    console.log(error)
  }
}

export default function * () {
  yield takeLatest(GET_SCOOTERS, submitGetScooters)
}
