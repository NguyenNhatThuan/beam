import { getTripsRequest } from '../../api/requests/trips'
import { call } from '../../api'
import { takeLatest, put } from 'redux-saga/effects'
import { GET_TRIPS } from '../../constants/types'
import { getTripsSuccessfully } from '../../screens/trip/action'

function * submitGetTrips (action) {
  try {
    const request = getTripsRequest()
    const response = yield call(request)
    yield put(getTripsSuccessfully(response.data))
  } catch (error) {
    console.log(error)
  }
}

export default function * () {
  yield takeLatest(GET_TRIPS, submitGetTrips)
}
