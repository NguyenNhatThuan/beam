import React from 'react'
import { createBottomTabNavigator } from 'react-navigation'
import HomeScreen from './screens/home'
import TripScreen from './screens/trip'
import Icon from 'react-native-ionicons'

export default createBottomTabNavigator(
  {
    Home: {
      screen: HomeScreen
    },
    Trip: {
      screen: TripScreen
    }
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state
        let iconName
        if (routeName === 'Home') {
          iconName = `map`
        } else if (routeName === 'Trip') {
          iconName = `information-circle${focused ? '' : '-outline'}`
        }

        return (
          <Icon name={iconName} size={horizontal ? 20 : 25} color={tintColor} />
        )
      }
    }),
    tabBarOptions: {
      activeTintColor: '#5022ff',
      inactiveTintColor: 'gray'
    }
  }
)
