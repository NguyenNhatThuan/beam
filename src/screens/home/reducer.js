import {
  UPDATE_GEOLOCATION,
  GET_SCOOTERS_SUCCESSFULLY
} from '../../constants/types'

const initState = {
  currentPosition: {},
  scooters: []
}

export default (state = initState, action) => {
  switch (action.type) {
    case UPDATE_GEOLOCATION:
      return {
        ...state,
        currentPosition: action.payload
      }

    case GET_SCOOTERS_SUCCESSFULLY:
      return {
        ...state,
        scooters: action.payload
      }

    default:
      return state
  }
}
