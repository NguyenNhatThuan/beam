import React from 'react'
import { View, Button, StyleSheet, Image, Text } from 'react-native'
import MapView, { Marker, Callout, PROVIDER_GOOGLE } from 'react-native-maps'
import PropTypes from 'prop-types'
import MapViewDirections from 'react-native-maps-directions'
import ScooterCallouts from '../../../components/scooterCallouts'
import { getDistance } from '../../../utils/geolocation'
import Icon from 'react-native-ionicons'

const HomePresentational = props => {
  const {
    scooters,
    currentCoord,
    destination,
    isDirection,
    onDirectionToMarker,
    onCancelDirection
  } = props
  return (
    <View style={{ flex: 1 }}>
      {currentCoord && (
        <MapView
          provider={PROVIDER_GOOGLE}
          style={{ flex: 1 }}
          initialRegion={currentCoord.coords}
        >
          {!isDirection &&
            currentCoord.coords &&
            scooters.length > 0 &&
            scooters.map((item, index) => {
              const distance = getDistance(currentCoord.coords, item.coordinate)
              return (
                <Marker
                  key={index}
                  coordinate={item.coordinate}
                  onCalloutPress={() => {
                    onDirectionToMarker && onDirectionToMarker(item)
                  }}
                >
                  <View style={styles.scooterMarkerContainer}>
                    <View style={{ flexDirection: 'row' }}>
                      <Icon
                        name="battery-charging"
                        style={styles.batteryIcon}
                      />
                      <Text style={styles.batteryView}>{item.battery}%</Text>
                    </View>
                    <Image
                      style={styles.imageMarker}
                      source={
                        item.imageUri
                          ? {
                            uri: item.imageUri
                          }
                          : require('../../../assets/images/defaultScooter.png')
                      }
                    />
                  </View>
                  <Callout style={styles.calloutContainer}>
                    <ScooterCallouts style={styles.scooterCallout} key={index}>
                      Serial Code:
                      <Text style={styles.infoText}>
                        {' '}
                        {item.serialCode.toUpperCase()}{' '}
                      </Text>
                      {'\n'}
                      Distance:
                      <Text style={styles.infoText}> {distance} KM</Text>
                    </ScooterCallouts>
                  </Callout>
                </Marker>
              )
            })}
          {isDirection && (
            <>
              <MapViewDirections
                origin={currentCoord.coords}
                destination={destination.coordinate}
                apikey="AIzaSyBYIWyg-eTxG4Fo1BiHPK5YEPMhWWlJhzY"
                strokeWidth={3}
                strokeColor="#5022ff"
              />
              <Marker coordinate={destination.coordinate}>
                <Image
                  style={styles.imageMarker}
                  source={
                    destination.imageUri
                      ? {
                        uri: destination.imageUri
                      }
                      : require('../../../assets/images/defaultScooter.png')
                  }
                />
              </Marker>
            </>
          )}
        </MapView>
      )}
      {isDirection && (
        <Callout style={styles.cancelDirectionContainer}>
          <Button
            style={styles.cancelDirectionButton}
            title="Cancel Direction"
            onPress={() => onCancelDirection && onCancelDirection()}
          />
        </Callout>
      )}
    </View>
  )
}

HomePresentational.propTypes = {
  scooters: PropTypes.array,
  currentCoord: PropTypes.object,
  destination: PropTypes.object,
  isDirection: PropTypes.bool,
  onDirectionToMarker: PropTypes.func,
  onCancelDirection: PropTypes.func
}

const styles = StyleSheet.create({
  calloutContainer: {
    flex: 1,
    width: 180
  },
  scooterCallout: {
    flex: 1,
    flexDirection: 'row',
    alignSelf: 'center'
  },
  scooterMarkerContainer: {
    flex: 1,
    alignContent: 'center',
    alignItems: 'center'
  },
  cancelDirectionContainer: {
    flexDirection: 'row',
    borderRadius: 10,
    width: '50%',
    marginLeft: '30%',
    marginRight: '30%',
    marginTop: 20
  },
  imageMarker: { width: 30, height: 47, alignSelf: 'center' },
  batteryView: {
    backgroundColor: '#5022ff',
    color: 'white',
    fontSize: 12,
    fontWeight: 'bold',
    borderRadius: 3,
    padding: 4,
    margin: 2
  },
  batteryIcon: {
    color: '#5022ff'
  },
  infoText: {
    color: '#5022ff',
    fontSize: 14,
    fontWeight: 'bold'
  }
})

export default HomePresentational
