export const selectScooters = state => {
  try {
    const scooters = state.user.scooters
    return scooters
  } catch (error) {
    return []
  }
}
