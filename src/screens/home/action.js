import {
  UPDATE_GEOLOCATION,
  GET_SCOOTERS,
  GET_SCOOTERS_SUCCESSFULLY
} from '../../constants/types'

export const updateGeolocation = position => ({
  type: UPDATE_GEOLOCATION,
  payload: position
})

export const getScooters = () => ({
  type: GET_SCOOTERS
})

export const getScootersSuccessfully = scooters => ({
  type: GET_SCOOTERS_SUCCESSFULLY,
  payload: scooters
})
