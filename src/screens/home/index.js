import React, { Component } from 'react'
import PropTypes from 'prop-types'
import HomePresentational from './components/index'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { updateGeolocation, getScooters } from './action'
import { getGeolocationUtil } from '../../utils/geolocation'
import { selectScooters } from './selectors'
import _ from 'lodash'
import { Alert } from 'react-native'

class HomeScreen extends Component {
  constructor (props) {
    super(props)

    this.state = {
      isDirection: false,
      destination: {}
    }
  }

  UNSAFE_componentWillMount = () => {
    getGeolocationUtil(
      position => {
        // get GPS first and store it then call get scooters around there
        this.props.updateGeolocation(position)
        this.props.getScooters(position)
      },
      error => {
        Alert.alert('Error', 'Could not get GPS')
      }
    )
  }

  onDirectionToMarker = item => {
    this.setState({
      isDirection: true,
      destination: item
    })
  }

  onCancelDirection = () => {
    this.setState({
      isDirection: false,
      destination: {}
    })
  }

  render () {
    return (
      <HomePresentational
        currentCoord={this.props.currentPosition}
        isDirection={this.state.isDirection}
        scooters={this.props.scooters}
        destination={this.state.destination}
        onDirectionToMarker={this.onDirectionToMarker}
        onCancelDirection={this.onCancelDirection}
      />
    )
  }
}

HomeScreen.propTypes = {
  updateGeolocation: PropTypes.func,
  getScooters: PropTypes.func,
  currentPosition: PropTypes.object,
  scooters: PropTypes.array
}

const mapStateToProps = state => ({
  currentPosition: state.user && state.user.currentPosition,
  scooters: selectScooters(state)
})

const mapDispatchToProps = dispatch =>
  bindActionCreators({ updateGeolocation, getScooters }, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeScreen)
