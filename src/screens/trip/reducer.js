import { GET_TRIPS_SUCCESSFULLY } from '../../constants/types'

const initState = {
  trips: []
}

export default (state = initState, action) => {
  switch (action.type) {
    case GET_TRIPS_SUCCESSFULLY:
      return action.payload

    default:
      return state
  }
}
