import React from 'react'
import { View, FlatList } from 'react-native'
import TripItem from './TripItem/index'
import TotalTrip from './TotalTrips/index'
import PropTypes from 'prop-types'

const TripPresentational = props => {
  const { totalDistance, trips } = props
  return (
    <View style={{ flex: 1 }}>
      <TotalTrip totalDistance={totalDistance} times={trips.length} />
      <FlatList data={trips} renderItem={TripItem} />
    </View>
  )
}

TripPresentational.propTypes = {
  trips: PropTypes.array,
  totalDistance: PropTypes.number
}

export default TripPresentational
