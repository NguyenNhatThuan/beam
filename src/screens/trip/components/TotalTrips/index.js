import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'

const TotalTrip = props => {
  const { totalDistance, times } = props
  return (
    <View style={styles.container}>
      <Text style={styles.numberText}>
        {totalDistance}/{times}
      </Text>
      <Text style={styles.normalText}>Total distance (km) / Times</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    padding: 16,
    backgroundColor: '#5022ff'
  },
  numberText: {
    color: '#FFFFFF',
    fontSize: 32
  },
  normalText: {
    color: '#FFFFFF',
    fontSize: 18
  }
})

TotalTrip.propTypes = {
  totalDistance: PropTypes.number,
  times: PropTypes.number
}

export default TotalTrip
