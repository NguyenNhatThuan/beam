import React from 'react'
import { Text, View, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'
import Icon from 'react-native-ionicons'
import CardView from 'react-native-cardview'

const TripItem = ({ item }) => {
  return (
    <CardView
      style={styles.tripItemContainer}
      cardElevation={1}
      cardMaxElevation={1}
      cornerRadius={3}
    >
      <View style={{ flexDirection: 'row', flex: 1 }}>
        <View style={styles.infoArea}>
          <Icon name="calendar" style={[styles.iconArea, styles.purple]} />
          <Text style={[styles.textStyle, styles.boldText, styles.black]}>
            {item.date.toLocaleDateString()}
          </Text>
        </View>
        <View style={styles.infoArea}>
          <Icon name="pricetag" style={[styles.iconArea, styles.purple]} />
          <Text style={[styles.textStyle, styles.boldText, styles.black]}>
            S ${item.price}
          </Text>
        </View>
      </View>
      <View style={{ flexDirection: 'row', flex: 1 }}>
        <View style={styles.infoArea}>
          <Text style={styles.textStyle}>{item.duration}</Text>
        </View>
        <View style={styles.infoArea}>
          <Icon name="pin" style={[styles.iconArea, styles.purple]} />
          <Text style={[styles.textStyle, styles.boldText, styles.black]}>
            {item.length} m
          </Text>
        </View>
      </View>
    </CardView>
  )
}

TripItem.propTypes = {
  item: PropTypes.object
}

const styles = StyleSheet.create({
  tripItemContainer: {
    padding: 4,
    marginHorizontal: 4,
    marginVertical: 2
  },
  infoArea: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  textStyle: {
    marginLeft: 4
  },
  iconArea: {
    width: '15%',
    alignSelf: 'center'
  },
  black: {
    color: '#000000'
  },
  gray: {
    color: '#D3D3D3'
  },
  purple: {
    color: '#5022ff'
  },
  boldText: {
    fontSize: 18
  }
})

export default TripItem
