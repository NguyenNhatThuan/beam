import { GET_TRIPS, GET_TRIPS_SUCCESSFULLY } from '../../constants/types'

export const getTrips = () => ({
  type: GET_TRIPS
})

export const getTripsSuccessfully = trips => ({
  type: GET_TRIPS_SUCCESSFULLY,
  payload: trips
})
