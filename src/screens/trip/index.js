import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import TripPresentational from './components/index'
import { getTrips } from './action'
import { selectTrips } from './selectors'
import _ from 'lodash'

class TripScreen extends Component {
  UNSAFE_componentWillMount = () => {
    this.props.getTrips()
  }

  render () {
    const totalDistance =
      this.props.trips.length > 0
        ? _.sumBy(this.props.trips, 'length') / 1000
        : 0

    return (
      <TripPresentational
        trips={this.props.trips}
        totalDistance={totalDistance}
      />
    )
  }
}
TripScreen.propTypes = {
  getTrips: PropTypes.func,
  trips: PropTypes.array
}
const mapStateToProps = state => ({
  trips: selectTrips(state)
})

const mapDispatchToProps = dispatch =>
  bindActionCreators({ getTrips }, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TripScreen)
