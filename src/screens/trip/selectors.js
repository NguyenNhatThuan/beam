export const selectTrips = state => {
  try {
    const trips = state.trips
    return trips.map(trip => {
      let date = new Date(trip.date)
      return {
        ...trip,
        date
      }
    })
  } catch (error) {
    return []
  }
}
