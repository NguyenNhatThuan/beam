import { combineReducers } from 'redux'
import homeReducer from './screens/home/reducer'
import tripReducer from './screens/trip/reducer'

export default combineReducers({
  user: homeReducer,
  trips: tripReducer
})
