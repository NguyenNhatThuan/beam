export const data = [
  {
    id: 'eacd879d-a0af-40b2-96d0-ec2c45502aa9',
    title: 'A',
    description: 'AA',
    serialCode:
      Math.random()
        .toString(36)
        .substring(2, 4) +
      Math.random()
        .toString(36)
        .substring(2, 4),
    battery: Math.floor(Math.random() * 100),
    imageUri:
      'https://target.scene7.com/is/image/Target/GUEST_ab26be96-d4cf-4216-bb15-cfab92bf6219?wid=488&hei=488&fmt=pjpeg',
    coordinate: {
      latitude: 10.775244,
      longitude: 106.702531,
      latitudeDelta: 0.015,
      longitudeDelta: 0.025
    }
  },
  {
    id: '7180c11c-6a0e-419c-b36e-69097df63e3e',
    title: 'B',
    description: 'BB',
    serialCode:
      Math.random()
        .toString(36)
        .substring(2, 4) +
      Math.random()
        .toString(36)
        .substring(2, 4),
    battery: Math.floor(Math.random() * 100),
    imageUri:
      'https://www.razor.com/wp-content/uploads/2018/01/A5DLX_SL_Product.png',
    coordinate: {
      latitude: 10.773349,
      longitude: 106.700844,
      latitudeDelta: 0.015,
      longitudeDelta: 0.025
    }
  },
  {
    id: '9609f43c-4047-43c7-b111-80ed5f7f90b5',
    title: 'C',
    description: 'CC',
    serialCode:
      Math.random()
        .toString(36)
        .substring(2, 4) +
      Math.random()
        .toString(36)
        .substring(2, 4),
    battery: Math.floor(Math.random() * 100),
    imageUri:
      'https://target.scene7.com/is/image/Target/GUEST_9a294250-bfa0-44f9-9215-a90c9fa44cbd?wid=488&hei=488&fmt=pjpeg',
    coordinate: {
      latitude: 10.772759,
      longitude: 106.698055,
      latitudeDelta: 0.015,
      longitudeDelta: 0.025
    }
  },
  {
    id: '34b26435-835b-4134-bc44-ae8effeba264',
    title: 'D',
    description: 'DD',
    serialCode:
      Math.random()
        .toString(36)
        .substring(2, 4) +
      Math.random()
        .toString(36)
        .substring(2, 4),
    battery: Math.floor(Math.random() * 100),
    imageUri:
      'http://i.ebayimg.com/00/s/MzY0WDM4MA==/z/NmgAAMXQlgtSyIDo/$_3.JPG?set_id=2',
    coordinate: {
      latitude: 10.773859,
      longitude: 106.698755,
      latitudeDelta: 0.015,
      longitudeDelta: 0.025
    }
  },
  {
    id: '3f27a034-1920-4215-9b62-c2f57b4c488a',
    title: 'E',
    description: 'EE',
    serialCode:
      Math.random()
        .toString(36)
        .substring(2, 4) +
      Math.random()
        .toString(36)
        .substring(2, 4),
    battery: Math.floor(Math.random() * 100),
    imageUri:
      'https://rockerbmx.com/images/stories/virtuemart/product/VIRALFUEL16%202.jpg',
    coordinate: {
      latitude: 10.774759,
      longitude: 106.695055,
      latitudeDelta: 0.015,
      longitudeDelta: 0.025
    }
  },
  {
    id: 'd96ff5f0-c609-42f7-a728-b15fe2604714',
    title: 'F',
    description: 'FF',
    serialCode:
      Math.random()
        .toString(36)
        .substring(2, 4) +
      Math.random()
        .toString(36)
        .substring(2, 4),
    battery: Math.floor(Math.random() * 100),
    coordinate: {
      latitude: 10.775769,
      longitude: 106.698055,
      latitudeDelta: 0.015,
      longitudeDelta: 0.025
    }
  },
  {
    id: '7919fb02-2cc1-44ce-8482-d72815f036d7',
    title: 'G',
    description: 'GG',
    serialCode:
      Math.random()
        .toString(36)
        .substring(2, 4) +
      Math.random()
        .toString(36)
        .substring(2, 4),
    battery: Math.floor(Math.random() * 100),
    coordinate: {
      latitude: 10.776759,
      longitude: 106.699555,
      latitudeDelta: 0.015,
      longitudeDelta: 0.025
    }
  },
  {
    id: 'e981fa9a-08cf-4ffc-8a7d-e13662d84df7',
    title: 'H',
    description: 'HH',
    serialCode:
      Math.random()
        .toString(36)
        .substring(2, 4) +
      Math.random()
        .toString(36)
        .substring(2, 4),
    battery: Math.floor(Math.random() * 100),
    coordinate: {
      latitude: 10.777739,
      longitude: 106.699055,
      latitudeDelta: 0.015,
      longitudeDelta: 0.025
    }
  },
  {
    id: '83315649-a7d9-4c86-9a61-2f86705fa28c',
    title: 'I',
    description: 'II',
    serialCode:
      Math.random()
        .toString(36)
        .substring(2, 4) +
      Math.random()
        .toString(36)
        .substring(2, 4),
    battery: Math.floor(Math.random() * 100),
    coordinate: {
      latitude: 10.778729,
      longitude: 106.696055,
      latitudeDelta: 0.015,
      longitudeDelta: 0.025
    }
  },
  {
    id: '1bed0c22-4c13-488e-a9e4-6499cf0085f2',
    title: 'K',
    description: 'KK',
    serialCode:
      Math.random()
        .toString(36)
        .substring(2, 4) +
      Math.random()
        .toString(36)
        .substring(2, 4),
    battery: Math.floor(Math.random() * 100),
    coordinate: {
      latitude: 10.779749,
      longitude: 106.699055,
      latitudeDelta: 0.015,
      longitudeDelta: 0.025
    }
  }
]
