const randomDate = (start, end) => {
  return new Date(
    start.getTime() + Math.random() * (end.getTime() - start.getTime())
  )
}

export const trips = [
  {
    id: '85be9e0c-87a5-4985-aa1e-ebd1b2929397',
    date: randomDate(new Date(2018, 0, 1), new Date()),
    duration: '10:23',
    length: Math.floor(Math.random() * 1000),
    vehicleId: 'eacd879d-a0af-40b2-96d0-ec2c45502aa9'
  },
  {
    id: '85be9e0c-87a5-4985-aa1e-ebd1b2929397',
    date: randomDate(new Date(2018, 0, 1), new Date()),
    duration: '20:23',
    length: Math.floor(Math.random() * 1000),
    vehicleId: 'eacd879d-a0af-40b2-96d0-ec2c45502aa9'
  },
  {
    id: '85be9e0c-87a5-4985-aa1e-ebd1b2929397',
    date: randomDate(new Date(2018, 0, 1), new Date()),
    duration: '30:23',
    length: Math.floor(Math.random() * 1000),
    vehicleId: 'eacd879d-a0af-40b2-96d0-ec2c45502aa9'
  },
  {
    id: '85be9e0c-87a5-4985-aa1e-ebd1b2929397',
    date: randomDate(new Date(2018, 0, 1), new Date()),
    duration: '5:23',
    length: Math.floor(Math.random() * 1000),
    vehicleId: 'eacd879d-a0af-40b2-96d0-ec2c45502aa9'
  },
  {
    id: '85be9e0c-87a5-4985-aa1e-ebd1b2929397',
    date: randomDate(new Date(2018, 0, 1), new Date()),
    duration: '4:23',
    length: Math.floor(Math.random() * 1000),
    vehicleId: 'eacd879d-a0af-40b2-96d0-ec2c45502aa9'
  },
  {
    id: '85be9e0c-87a5-4985-aa1e-ebd1b2929397',
    date: randomDate(new Date(2018, 0, 1), new Date()),
    duration: '10:23',
    length: Math.floor(Math.random() * 1000),
    vehicleId: 'eacd879d-a0af-40b2-96d0-ec2c45502aa9'
  },
  {
    id: '85be9e0c-87a5-4985-aa1e-ebd1b2929397',
    date: randomDate(new Date(2018, 0, 1), new Date()),
    duration: '10:23',
    length: Math.floor(Math.random() * 1000),
    vehicleId: 'eacd879d-a0af-40b2-96d0-ec2c45502aa9'
  },
  {
    id: '85be9e0c-87a5-4985-aa1e-ebd1b2929397',
    date: randomDate(new Date(2018, 0, 1), new Date()),
    duration: '11:23',
    length: Math.floor(Math.random() * 1000),
    vehicleId: 'eacd879d-a0af-40b2-96d0-ec2c45502aa9'
  },
  {
    id: '85be9e0c-87a5-4985-aa1e-ebd1b2929397',
    date: randomDate(new Date(2018, 0, 1), new Date()),
    duration: '10:23',
    length: Math.floor(Math.random() * 1000),
    vehicleId: 'eacd879d-a0af-40b2-96d0-ec2c45502aa9'
  },
  {
    id: '85be9e0c-87a5-4985-aa1e-ebd1b2929397',
    date: randomDate(new Date(2018, 0, 1), new Date()),
    duration: '20:22',
    length: Math.floor(Math.random() * 1000),
    vehicleId: 'eacd879d-a0af-40b2-96d0-ec2c45502aa9'
  },
  {
    id: '85be9e0c-87a5-4985-aa1e-ebd1b2929397',
    date: randomDate(new Date(2018, 0, 1), new Date()),
    duration: '19:21',
    length: Math.floor(Math.random() * 1000),
    vehicleId: 'eacd879d-a0af-40b2-96d0-ec2c45502aa9'
  },
  {
    id: '85be9e0c-87a5-4985-aa1e-ebd1b2929397',
    date: randomDate(new Date(2018, 0, 1), new Date()),
    duration: '13:53',
    length: Math.floor(Math.random() * 1000),
    vehicleId: 'eacd879d-a0af-40b2-96d0-ec2c45502aa9'
  },
  {
    id: '85be9e0c-87a5-4985-aa1e-ebd1b2929397',
    date: randomDate(new Date(2018, 0, 1), new Date()),
    duration: '17:23',
    length: Math.floor(Math.random() * 1000),
    vehicleId: 'eacd879d-a0af-40b2-96d0-ec2c45502aa9'
  },
  {
    id: '85be9e0c-87a5-4985-aa1e-ebd1b2929397',
    date: randomDate(new Date(2018, 0, 1), new Date()),
    duration: '13:23',
    length: Math.floor(Math.random() * 1000),
    vehicleId: 'eacd879d-a0af-40b2-96d0-ec2c45502aa9'
  },
  {
    id: '85be9e0c-87a5-4985-aa1e-ebd1b2929397',
    date: randomDate(new Date(2018, 0, 1), new Date()),
    duration: '32:23',
    length: Math.floor(Math.random() * 1000),
    vehicleId: 'eacd879d-a0af-40b2-96d0-ec2c45502aa9'
  },
  {
    id: '85be9e0c-87a5-4985-aa1e-ebd1b2929397',
    date: randomDate(new Date(2018, 0, 1), new Date()),
    duration: '40:33',
    length: Math.floor(Math.random() * 1000),
    vehicleId: '7180c11c-6a0e-419c-b36e-69097df63e3e'
  },
  {
    id: '85be9e0c-87a5-4985-aa1e-ebd1b2929397',
    date: randomDate(new Date(2018, 0, 1), new Date()),
    duration: '16:00',
    length: Math.floor(Math.random() * 1000),
    vehicleId: '7180c11c-6a0e-419c-b36e-69097df63e3e'
  },
  {
    id: '85be9e0c-87a5-4985-aa1e-ebd1b2929397',
    date: randomDate(new Date(2018, 0, 1), new Date()),
    duration: '14:29',
    length: Math.floor(Math.random() * 1000),
    vehicleId: '7180c11c-6a0e-419c-b36e-69097df63e3e'
  },
  {
    id: '85be9e0c-87a5-4985-aa1e-ebd1b2929397',
    date: randomDate(new Date(2018, 0, 1), new Date()),
    duration: '18:23',
    length: Math.floor(Math.random() * 1000),
    vehicleId: '7180c11c-6a0e-419c-b36e-69097df63e3e'
  },
  {
    id: '85be9e0c-87a5-4985-aa1e-ebd1b2929397',
    date: randomDate(new Date(2018, 0, 1), new Date()),
    duration: '13:45',
    length: Math.floor(Math.random() * 1000),
    vehicleId: '7180c11c-6a0e-419c-b36e-69097df63e3e'
  },
  {
    id: '85be9e0c-87a5-4985-aa1e-ebd1b2929397',
    date: randomDate(new Date(2018, 0, 1), new Date()),
    duration: '12:24',
    length: Math.floor(Math.random() * 1000),
    vehicleId: '7180c11c-6a0e-419c-b36e-69097df63e3e'
  },
  {
    id: '85be9e0c-87a5-4985-aa1e-ebd1b2929397',
    date: randomDate(new Date(2018, 0, 1), new Date()),
    duration: '20:21',
    length: Math.floor(Math.random() * 1000),
    vehicleId: '7180c11c-6a0e-419c-b36e-69097df63e3e'
  },
  {
    id: '85be9e0c-87a5-4985-aa1e-ebd1b2929397',
    date: randomDate(new Date(2018, 0, 1), new Date()),
    duration: '1:23',
    length: Math.floor(Math.random() * 1000),
    vehicleId: '7180c11c-6a0e-419c-b36e-69097df63e3e'
  }
]
